Písemka 1
================================================================

Při řešení tohoto úkolu nejsou povoleny žádné pomůcky ani Internety ani staré
zdrojové kódy. Vše řešte samostatně pouze s tím, co máte v hlavě a v rukách.



Úloha 1
----------------------------------------------------------------

Vytvořte pole `cisla` a vložte do něj tato čísla v různých číselných
soustavách. Poli zvolte vhodný datový typ, aby se čísla vložila správně -- bez
přetečení.

300 D,  10101010101 B,  777 O,  F2C H

Úloha 2
----------------------------------------------------------------

Vytvořte pole `nasobky` a vložte do něj všechny násobky čísla 7 menší než 200.

Úloha 3 
----------------------------------------------------------------

Z pole `nasobky` vypište všechna čísla větší než 50 a menší než 150. Vypište je
do řady -- na jeden řádek.


Úloha 4 
----------------------------------------------------------------

Z pole `nasobky` vypište všechna čísla dělitelná číslem 3. Vypište každé číslo
na samostatný řádek.

Pokud jste v předchozí úloze použili cyklus for použijte pro tuto úlohu cyklus
while. Pokud jste v předchozí úloze použili cyklus while, použijte cyklus for.


Úloha 5 
----------------------------------------------------------------

Vytvořte řetězec `text`, kterému alokujete paměť na 200 znaků a vložíte do něj
"Zdravim kazdeho kamose." Pomocí cyklu nahraďte všechny znaky 'a' znakem '@' a
text vypište na obrazovku.


Úloha 6
----------------------------------------------------------------

Máme proměnnou `cislo` o velikosti 4B obsahující číslo 12345678 H. Rozdělte tuto
proměnnou na jednotlivé Byte -- tedy 12 H, 34 H, 56 H a 78H.


Úloha 7
----------------------------------------------------------------

Uvědomte si, jak je úžasné, že umíte používat jazyk C a napište slohovou práci
opěvující tento skvělí nástroj. Práce musí obsahovat alespoň 7 slov a musí se
rozkládat alespoň na dvou řádcích. Tuto práci vepište jako komentář do
zdrojového kódu.
