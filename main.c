#include <stdio.h>

int main(void) {
    // int cisla[] = { 300, 0b101010101, 0777, 0xF2C };
    int cisla[4] = {300, 0b101010101, 0777, 0xF2C};
    /*
    int cisla[4];
    cisla[0] = 300;
    cisla[1] = 0b101010101;
    cisla[2] = 0777;
    cisla[3] = 0xF2C; */
    printf("cisla = %d %b %o %X \n", cisla[0], cisla[1], cisla[2], cisla[3]);

    /* Jazyk C je super
       fakt hrozně moc super  */

    int nasobky[30]; // pole násobky má 30 položek
    // všechny násobky čísla 7 menší než 200
    int i = 0;
    int cislo = 7;
    while (cislo < 200) {
        nasobky[i] = cislo;
        // printf("%d %d\n", i, cislo);
        cislo += 7;
        i += 1;
    }

    // z pole `násobky` vypište všechna čísla větší než 50 a menší než 150.
    for (i = 0; i < 30; i++) {
        if (nasobky[i] > 50 && nasobky[i] < 150) {
            printf("%d ", nasobky[i]);
        }
    }
    putchar('\n');

    // Z pole `násobky` vypište všechna čísla dělitelná číslem 3.
    i = 0;
    while (i < 30) {
        if (nasobky[i] % 3 == 0) {
            printf("%d\n", nasobky[i]);
        }
        i += 1;
    }

    /*Vytvořte řetězec `text`, kterému alokujete paměť na 200 znaků a vložíte do
    něj "Zdravim kazdeho kamose." Pomocí cyklu nahraďte všechny znaky 'a' znakem
    '@' a text vypište na obrazovku.*/
    char text[200] = "Zdravim kazdeho kamose.";
    for (i = 0; text[i] != '\0'; i++) {
        if (text[i] == 'a') {
            text[i] = '@';
        }
    }
    printf("%s\n", text);

    /* Máme proměnnou `cislo` o velikosti 4B obsahující číslo 12345678 H.
    Rozdělte tuto proměnnou na jednotlivé Byte:
    tedy 12 H, 34 H, 56 H a 78H. */
    cislo = 0x12345678;
    char a, b, c, d;
    a = (cislo & 0xFF000000) >> 24;
    b = (cislo & 0x00FF0000) >> 16;
    c = (cislo & 0x0000FF00) >> 8;
    //d = (cislo & 0x000000FF) >> 0;
    d = (cislo & 0x000000FF);
    printf("%X %X %X %X\n", a, b, c, d);
    printf("%d %d %d %d\n", a, b, c, d);
}
